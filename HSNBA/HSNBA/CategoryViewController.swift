
//
//  CategoryViewController.swift
//  HSNBA
//
//  Created by leobs on 09.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation
import UIKit

class CategoryViewController:AnalyticsViewController, UITableViewDataSource, UITableViewDelegate{

    
    
    var myArray = ["AAA", "BBB", "CCC", "DDD", "FFF"]
    var categoriesArray = Array<Category>()
    static var worldScores = [120, 103, 108, 89, 134, 97, 125, 131, 110]
    
    let iwBackground : UIImageView = {
        let iwBackground = UIImageView(image: #imageLiteral(resourceName: "main_page"))
        iwBackground.translatesAutoresizingMaskIntoConstraints = false
        return iwBackground
    }()
    
    let upBarView : UIView = {
        let upBarView = UIView()
        upBarView.translatesAutoresizingMaskIntoConstraints = false
        return upBarView
    }()
    
    let twChooseCategory : UILabel = {
        let twChooseCategory = UILabel()
        twChooseCategory.translatesAutoresizingMaskIntoConstraints = false
        twChooseCategory.text = "Choose category"
        twChooseCategory.backgroundColor = .clear
        twChooseCategory.font = UIFont.boldSystemFont(ofSize: 20)
        twChooseCategory.textColor = .white
        twChooseCategory.textAlignment = .center
        return twChooseCategory
    }()
    
    let twBackBtn : UILabel = {
        let twBackBtn = UILabel()
        twBackBtn.translatesAutoresizingMaskIntoConstraints = false
        twBackBtn.text = "Back"
        twBackBtn.backgroundColor = .clear
        twBackBtn.font = UIFont.boldSystemFont(ofSize: 14)
        twBackBtn.textColor = .white
        twBackBtn.textAlignment = .left
        return twBackBtn
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tableView = UITableView(frame: view.bounds, style: UITableViewStyle.grouped)
        tableView.delegate = self
        tableView.dataSource = self
       
        view.addSubview(iwBackground)
        tableView.backgroundView = iwBackground
        view.addSubview(tableView)
        view.addSubview(upBarView)

        for index in 1...9 {
            let categoryUserScore = SharedPreferences.getIntValueForKey(key: SharedPreferences.HIGHSCORE + String(index))
            let category = Category(categoryId: index, categoryName: CategoryHelper.getCategoryFullNameTitleCase(category: index), categoryWorldScore: CategoryViewController.worldScores[index-1] ,categoryUserScore: categoryUserScore)
            categoriesArray.append(category)
        }
        
        
        upBarView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        upBarView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        upBarView.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        upBarView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        upBarView.backgroundColor = UIColor(red:15/255, green: 117/255, blue: 189/255,alpha: 1.0)
        
        upBarView.addSubview(twChooseCategory)
        twChooseCategory.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twChooseCategory.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        twChooseCategory.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twChooseCategory.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        
        upBarView.addSubview(twBackBtn)
        twBackBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twBackBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        twBackBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: self.view.frame.size.width/30).isActive = true
        twBackBtn.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        
        
        iwBackground.topAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        iwBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        iwBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        iwBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
       
        upBarView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backBtnClicked(_:)))
        twBackBtn.isUserInteractionEnabled = true
        twBackBtn.addGestureRecognizer(tap)
    }
    
    @objc func backBtnClicked(_ sender: UITapGestureRecognizer){
        let touchPoint = sender.location(in: self.view)
        let backBtnClicked = (touchPoint.x < self.view.frame.width/4)
        if(backBtnClicked){
            let startViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
            self.present(startViewController, animated: true, completion: nil)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.size.height / 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = MyTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "myIdentifier")
        cell.twLeagueName.text = "NBA"
        cell.twCategoryName.text = categoriesArray[indexPath.row].categoryName
        cell.twUserScore.text = "Your Score : " + String(categoriesArray[indexPath.row].categoryUserScore!)
        cell.twWorldScore.text = "World Score : " + String(categoriesArray[indexPath.row].categoryWorldScore!)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SharedPreferences.setIntValueForKey(key: SharedPreferences.CURRENT_CATEGORY, value: indexPath.row + 1)
        let startViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
        sendEvent(category: "CATEGORY" , action: "CATEGORY" + String(indexPath.row + 1), label: "")
        self.present(startViewController, animated: true, completion: nil)
        print()
    }
    
    
    @objc func pressedBrowser(sender: UIButton) {
        print("pressedBrowser")
    }
    
    @objc func pressedTelephone(sender: UIButton) {
        print("pressedTelephone")
    }
}
