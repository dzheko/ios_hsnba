//
//
// DAO Object for Player
//  @author Dzh
//  @since 06.03.2018
//

import Foundation

class PlayerDAO{
    
    let CATEGORY1_ = "player_minutes_per_game_"
    let CATEGORY2_ = "player_points_per_game_"
    let CATEGORY3_ = "player_points_per48_"
    let CATEGORY4_ = "player_field_goals_per_game_"
    let CATEGORY5_ = "player_field_goals_percentage_"
    let CATEGORY6_ = "player_three_points_per_game_"
    let CATEGORY7_ = "player_free_throws_per_game_"
    let CATEGORY8_ = "player_free_throws_percentage_"
    let CATEGORY9_ = "player_points_per_shot_"
    
    init(){}
    
    /**
     * Creates Player
     */
    public func getPlayerData(categoryToPlayNumber : Int) -> Player {
        
        let randomNumber =  arc4random_uniform(450) + 0
        
        
        let PLAYER_TEAM = "player_club_" + String(randomNumber)
        let PLAYER_NAME = "player_name_" + String(randomNumber)
        
        let CATEGORY1 =  CATEGORY1_ + String(randomNumber)
        let CATEGORY2 =  CATEGORY2_ + String(randomNumber)
        let CATEGORY3 =  CATEGORY3_ + String(randomNumber)
        let CATEGORY4 =  CATEGORY4_ + String(randomNumber)
        let CATEGORY5 =  CATEGORY5_ + String(randomNumber)
        let CATEGORY6 =  CATEGORY6_ + String(randomNumber)
        let CATEGORY7 =  CATEGORY7_ + String(randomNumber)
        let CATEGORY8 =  CATEGORY8_ + String(randomNumber)
        let CATEGORY9 =  CATEGORY9_ + String(randomNumber)
    
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY1, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY2, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY3, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY4, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY5, comment: ""))
        let category6Stat = Double(NSLocalizedString(CATEGORY6, comment: ""))
        let category7Stat = Double(NSLocalizedString(CATEGORY7, comment: ""))
        let category8Stat = Double(NSLocalizedString(CATEGORY8, comment: ""))
        let category9Stat = Double(NSLocalizedString(CATEGORY9, comment: ""))
        
        let player = Player(playerName: playerName, teamName : playerTeam)
        
        switch categoryToPlayNumber {
            case 1:  player.playerCategoryStatToPlay = category1Stat; break;
            case 2:  player.playerCategoryStatToPlay = category2Stat; break;
            case 3:  player.playerCategoryStatToPlay = category3Stat; break;
            case 4:  player.playerCategoryStatToPlay = category4Stat; break;
            case 5:  player.playerCategoryStatToPlay = category5Stat; break;
            case 6:  player.playerCategoryStatToPlay = category6Stat; break;
            case 7:  player.playerCategoryStatToPlay = category7Stat; break;
            case 8:  player.playerCategoryStatToPlay = category8Stat; break;
            case 9:  player.playerCategoryStatToPlay = category9Stat; break;
            default: player.playerCategoryStatToPlay = 0; break;
        }
        
        return player
    }
    
    
    
}
