//
//  Player.swift
//  HSNBA
//
//  Created by leobs on 06.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class Player {
    
    var playerName: String?
    var teamName: String?
    
    var playerCategory1Stat: Double?
    var playerCategory2Stat: Double?
    var playerCategory3Stat: Double?
    var playerCategory4Stat: Double?
    var playerCategory5Stat: Double?
    var playerCategory6Stat: Double?
    var playerCategory7Stat: Double?
    var playerCategory8Stat: Double?
    var playerCategory9Stat: Double?
    
    var playerCategoryStatToPlay: Double?
    
    init(playerName: String, teamName: String){
        self.playerName = playerName
        self.teamName = teamName
    }
    
}
