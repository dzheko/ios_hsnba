//
//  AbstractViewController.swift
//  HSNBA
//
//  Created by leobs on 06.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AbstractViewController: AnalyticsViewController{
   
    let upBarView : UIView = {
        let upBarView = UIView()
        upBarView.translatesAutoresizingMaskIntoConstraints = false
        return upBarView
    }()
    
    let iwFirstPlayer : UIImageView = {
        let iwFirstPlayer = UIImageView(image: #imageLiteral(resourceName: "back_up"))
        iwFirstPlayer.translatesAutoresizingMaskIntoConstraints = false
        return iwFirstPlayer
    }()
    
    let iwSecondPlayer : UIImageView = {
        let iwSecondPlayer = UIImageView(image:#imageLiteral(resourceName: "back_down"))
        iwSecondPlayer.translatesAutoresizingMaskIntoConstraints = false
        return iwSecondPlayer
    }()
    
    
    let twScore : UILabel = {
        let twScore = UILabel()
        twScore.text = "score : 25"
        twScore.font = UIFont.boldSystemFont(ofSize: 14)
        twScore.textAlignment = .center
        twScore.textColor = .white
        twScore.translatesAutoresizingMaskIntoConstraints = false
        return twScore
    }()
    
    let twHighScore : UILabel = {
        let twScore = UILabel()
        twScore.text = "highscore : 25"
        twScore.font = UIFont.boldSystemFont(ofSize: 14)
        twScore.textAlignment = .right
        twScore.textColor = .white
        twScore.translatesAutoresizingMaskIntoConstraints = false
        return twScore
    }()
    
    let twBackBtn : UILabel = {
        let back = UILabel()
        back.text = "Back"
        back.font = UIFont.boldSystemFont(ofSize: 14)
        back.textAlignment = .left
        back.textColor = .white
        back.translatesAutoresizingMaskIntoConstraints = false
        return back
    }()
    
    let btnHigher = UIButton()
    let btnLower = UIButton()
    var iwVs = UIImageView(image: #imageLiteral(resourceName: "vs_image"))
    var currentVsSign = ""
    
    
    
    var twPlayerOneName = UILabel()
    var twPlayerOneTeam = UILabel()
    var twPlayerOneVerb = UILabel()
    var twPlayerOneStat = UILabel()
    var twPlayerOneCategory = UILabel()
    var iwPlayerOneLogo = UIImageView()
    
    var twPlayerTwoName = UILabel()
    var twPlayerTwoTeam = UILabel()
    var twPlayerTwoVerb = UILabel()
    var twPlayerTwoStat = EFCountingLabel()
    var twPlayerTwoCategory = UILabel()
    var iwPlayerTwoLogo = UIImageView()
    
    let twHigher = UILabel()
    let twLower = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
     * Creates Up Bar
     */
    public func uiCreateUpBar(){
        upBarView.isUserInteractionEnabled = true
        upBarView.backgroundColor = UIColor(red:15/255, green: 117/255, blue: 189/255,alpha: 1.0)
        upBarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upBarView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        upBarView.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 15).isActive = true
        upBarView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backBtnClicked(_:)))
        twBackBtn.isUserInteractionEnabled = true
        twBackBtn.addGestureRecognizer(tap)
        
        upBarView.addSubview(twScore)
        upBarView.addSubview(twHighScore)
        upBarView.addSubview(twBackBtn)
        
        twScore.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twScore.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        twScore.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twScore.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor, constant: 10).isActive = true
        
        twHighScore.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twHighScore.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        twHighScore.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twHighScore.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor, constant: 10).isActive = true
        
        twBackBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant:self.view.frame.size.width/30).isActive = true
        twBackBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        twBackBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twBackBtn.bottomAnchor.constraint(equalTo: upBarView.bottomAnchor, constant: 10).isActive = true
        
        
        
        twBackBtn.backgroundColor = UIColor.clear
        twHighScore.backgroundColor = UIColor.clear
        twScore.backgroundColor = UIColor.clear
        
    }
    
    /**
     * creates First Player Layout
     */
    public func uiCreateFirstPlayerLayout(){
        iwFirstPlayer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwFirstPlayer.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        iwFirstPlayer.topAnchor.constraint(equalTo: upBarView.bottomAnchor).isActive = true
        iwFirstPlayer.heightAnchor.constraint(equalToConstant: self.view.frame.size.height * 6.5 / 15).isActive = true
        
        iwFirstPlayer.addSubview(iwPlayerOneLogo)
        
        iwPlayerOneLogo.translatesAutoresizingMaskIntoConstraints = false
        iwPlayerOneLogo.rightAnchor.constraint(equalTo: iwFirstPlayer.rightAnchor, constant: -3).isActive = true
        iwPlayerOneLogo.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        iwPlayerOneLogo.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        iwPlayerOneLogo.topAnchor.constraint(equalTo: iwFirstPlayer.topAnchor,constant: 3).isActive = true
        
        
        iwFirstPlayer.addSubview(twPlayerOneTeam)
        
        twPlayerOneTeam.translatesAutoresizingMaskIntoConstraints = false
        twPlayerOneTeam.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: self.view.frame.size.width/40).isActive = true
        twPlayerOneTeam.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerOneTeam.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerOneTeam.topAnchor.constraint(equalTo: upBarView.bottomAnchor,constant: 0).isActive = true
        twPlayerOneTeam.font = UIFont.boldSystemFont(ofSize: 16)
        twPlayerOneTeam.textColor = .white
        twPlayerOneTeam.backgroundColor = UIColor.clear
        
        iwFirstPlayer.addSubview(twPlayerOneName)
        
        twPlayerOneName.translatesAutoresizingMaskIntoConstraints = false
        twPlayerOneName.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerOneName.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerOneName.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerOneName.topAnchor.constraint(equalTo: iwPlayerOneLogo.bottomAnchor,constant: 0).isActive = true
        
        twPlayerOneName.font = UIFont.boldSystemFont(ofSize: 26)
        twPlayerOneName.textAlignment = .center
        twPlayerOneName.textColor = .white
        twPlayerOneName.backgroundColor = UIColor.clear
        
        
        iwFirstPlayer.addSubview(twPlayerOneVerb)
        
        twPlayerOneVerb.translatesAutoresizingMaskIntoConstraints = false
        twPlayerOneVerb.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerOneVerb.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerOneVerb.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerOneVerb.topAnchor.constraint(equalTo: twPlayerOneName.bottomAnchor,constant: 0).isActive = true
        twPlayerOneVerb.font = UIFont.boldSystemFont(ofSize: 18)
        twPlayerOneVerb.textAlignment = .center
        twPlayerOneVerb.textColor = .white
        twPlayerOneVerb.backgroundColor = UIColor.clear
        
        iwFirstPlayer.addSubview(twPlayerOneStat)
        
        twPlayerOneStat.translatesAutoresizingMaskIntoConstraints = false
        twPlayerOneStat.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerOneStat.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerOneStat.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twPlayerOneStat.topAnchor.constraint(equalTo: twPlayerOneVerb.bottomAnchor,constant: 0).isActive = true
        twPlayerOneStat.font = UIFont.boldSystemFont(ofSize: 38)
        twPlayerOneStat.textAlignment = .center
        twPlayerOneStat.textColor = .yellow
        twPlayerOneStat.backgroundColor = UIColor.clear
        
        iwFirstPlayer.addSubview(twPlayerOneCategory)
        
        twPlayerOneCategory.translatesAutoresizingMaskIntoConstraints = false
        twPlayerOneCategory.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerOneCategory.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerOneCategory.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerOneCategory.topAnchor.constraint(equalTo: twPlayerOneStat.bottomAnchor,constant: 0).isActive = true
        twPlayerOneCategory.font = UIFont.boldSystemFont(ofSize: 22)
        twPlayerOneCategory.textAlignment = .center
        twPlayerOneCategory.textColor = .white
        twPlayerOneCategory.backgroundColor = UIColor.clear
        
        uiCreateVsPart()
    }
    
    /*
     * creates Vs Part
     */
    public func uiCreateVsPart(){
        iwVs.translatesAutoresizingMaskIntoConstraints = false
        iwFirstPlayer.addSubview(iwVs)
        
        iwVs.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwVs.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 10).isActive = true
        iwVs.bottomAnchor.constraint(equalTo: iwFirstPlayer.bottomAnchor, constant: iwVs.frame.size.width/5).isActive = true
        iwVs.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 10).isActive = true
        
        
        
    }
    
    /*
     * creates Second Player
     */
    public func uiCreateSecondPlayerLayout(){
        iwSecondPlayer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwSecondPlayer.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        iwSecondPlayer.topAnchor.constraint(equalTo: iwFirstPlayer.bottomAnchor).isActive = true
        iwSecondPlayer.heightAnchor.constraint(equalToConstant: self.view.frame.size.height * 7.5 / 15).isActive = true
        iwSecondPlayer.layer.zPosition = -1
        
        iwSecondPlayer.addSubview(iwPlayerTwoLogo)
        
        iwPlayerTwoLogo.translatesAutoresizingMaskIntoConstraints = false
        iwPlayerTwoLogo.leftAnchor.constraint(equalTo: iwSecondPlayer.leftAnchor, constant: 3).isActive = true
        iwPlayerTwoLogo.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        iwPlayerTwoLogo.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        iwPlayerTwoLogo.topAnchor.constraint(equalTo: iwSecondPlayer.topAnchor,constant: 3).isActive = true
        
        iwSecondPlayer.addSubview(twPlayerTwoTeam)
        
        twPlayerTwoTeam.translatesAutoresizingMaskIntoConstraints = false
        twPlayerTwoTeam.rightAnchor.constraint(equalTo: iwSecondPlayer.rightAnchor, constant: -self.view.frame.size.width/40).isActive = true
        twPlayerTwoTeam.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerTwoTeam.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerTwoTeam.topAnchor.constraint(equalTo: iwSecondPlayer.topAnchor,constant: 0).isActive = true
        twPlayerTwoTeam.font = UIFont.boldSystemFont(ofSize: 16)
        twPlayerTwoTeam.textColor = .white
        twPlayerTwoTeam.textAlignment = .right
        twPlayerTwoTeam.backgroundColor = UIColor.clear
        
        
        iwSecondPlayer.addSubview(twPlayerTwoName)
        
        twPlayerTwoName.translatesAutoresizingMaskIntoConstraints = false
        twPlayerTwoName.leftAnchor.constraint(equalTo: iwFirstPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerTwoName.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerTwoName.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerTwoName.topAnchor.constraint(equalTo: iwPlayerTwoLogo.bottomAnchor,constant: 0).isActive = true
        
        twPlayerTwoName.font = UIFont.boldSystemFont(ofSize: 26)
        twPlayerTwoName.textAlignment = .center
        twPlayerTwoName.textColor = .white
        twPlayerTwoName.backgroundColor = UIColor.clear
        
        
        iwSecondPlayer.addSubview(twPlayerTwoVerb)
        
        twPlayerTwoVerb.translatesAutoresizingMaskIntoConstraints = false
        twPlayerTwoVerb.leftAnchor.constraint(equalTo: iwSecondPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerTwoVerb.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerTwoVerb.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerTwoVerb.topAnchor.constraint(equalTo: twPlayerTwoName.bottomAnchor,constant: 0).isActive = true
        twPlayerTwoVerb.font = UIFont.boldSystemFont(ofSize: 18)
        twPlayerTwoVerb.textAlignment = .center
        twPlayerTwoVerb.textColor = .white
        twPlayerTwoVerb.backgroundColor = UIColor.clear
        
        
        
        iwSecondPlayer.addSubview(twPlayerTwoStat)
        
        twPlayerTwoStat.translatesAutoresizingMaskIntoConstraints = false
        twPlayerTwoStat.leftAnchor.constraint(equalTo: iwSecondPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerTwoStat.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerTwoStat.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twPlayerTwoStat.topAnchor.constraint(equalTo: twPlayerTwoVerb.bottomAnchor,constant: 0).isActive = true
        twPlayerTwoStat.font = UIFont.boldSystemFont(ofSize: 38)
        twPlayerTwoStat.textAlignment = .center
        twPlayerTwoStat.textColor = .yellow
        twPlayerTwoStat.backgroundColor = UIColor.clear
        
        createUpButton(view: twPlayerTwoVerb)
        createDownButton(view: btnHigher)
        
        
        
        iwSecondPlayer.addSubview(twPlayerTwoCategory)
        
        twPlayerTwoCategory.translatesAutoresizingMaskIntoConstraints = false
        twPlayerTwoCategory.leftAnchor.constraint(equalTo: iwSecondPlayer.leftAnchor, constant: 0).isActive = true
        twPlayerTwoCategory.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true
        twPlayerTwoCategory.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/20).isActive = true
        twPlayerTwoCategory.topAnchor.constraint(equalTo: btnLower.bottomAnchor,constant: 0).isActive = true
        twPlayerTwoCategory.font = UIFont.boldSystemFont(ofSize: 22)
        twPlayerTwoCategory.textAlignment = .center
        twPlayerTwoCategory.textColor = .white
        twPlayerTwoCategory.backgroundColor = UIColor.clear
        
        
    }
    
    /**
     * Creates Up Button
     */
    public func createUpButton(view : UIView){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.upDownListener(_:)))
        btnHigher.addGestureRecognizer(tap)
        iwSecondPlayer.isUserInteractionEnabled = true
        
        iwSecondPlayer.addSubview(btnHigher)
        btnHigher.translatesAutoresizingMaskIntoConstraints = false
        btnHigher.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btnHigher.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        btnHigher.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        btnHigher.topAnchor.constraint(equalTo: view.bottomAnchor,constant: 3).isActive = true
        btnHigher.backgroundColor = .white
        btnHigher.layer.cornerRadius = 10
        btnHigher.layer.borderWidth = 2
        btnHigher.layer.borderColor = UIColor(red:19/255, green: 107/255, blue: 4/255,alpha: 1.0).cgColor
        
        let iwUp = UIImageView(image:#imageLiteral(resourceName: "corner_up"))
        btnHigher.addSubview(iwUp)
        iwUp.translatesAutoresizingMaskIntoConstraints = false
        iwUp.leftAnchor.constraint(equalTo: btnHigher.leftAnchor, constant: self.view.frame.size.width / 35).isActive = true
        iwUp.widthAnchor.constraint(equalToConstant:self.view.frame.size.width / 12).isActive = true
        iwUp.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 30).isActive = true
        iwUp.centerYAnchor.constraint(equalTo: btnHigher.centerYAnchor,constant: 0).isActive = true
        
        
        btnHigher.addSubview(twHigher)
        
        twHigher.translatesAutoresizingMaskIntoConstraints = false
        twHigher.leftAnchor.constraint(equalTo: iwUp.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twHigher.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 3).isActive = true
        twHigher.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twHigher.centerYAnchor.constraint(equalTo: btnHigher.centerYAnchor,constant: 0).isActive = true
        twHigher.font = UIFont.boldSystemFont(ofSize: 25)
        twHigher.textAlignment = .center
        twHigher.textColor = UIColor(red:19/255, green: 107/255, blue: 4/255,alpha: 1.0)
        twHigher.backgroundColor = UIColor.clear
    }
    
    /**
     * Creates Down Button
     */
    public func createDownButton(view : UIView){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.upDownListener(_:)))
        btnLower.addGestureRecognizer(tap)
        iwSecondPlayer.isUserInteractionEnabled = true
        
        iwSecondPlayer.addSubview(btnLower)
        btnLower.translatesAutoresizingMaskIntoConstraints = false
        btnLower.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btnLower.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        btnLower.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        btnLower.topAnchor.constraint(equalTo: view.bottomAnchor,constant: 3).isActive = true
        btnLower.backgroundColor = .white
        btnLower.layer.cornerRadius = 10
        btnLower.layer.borderWidth = 2
        btnLower.layer.borderColor = UIColor(red:163/255, green: 1/255, blue: 1/255,alpha: 1.0).cgColor
        
        let iwDown = UIImageView(image:#imageLiteral(resourceName: "corner_down"))
        btnLower.addSubview(iwDown)
        iwDown.translatesAutoresizingMaskIntoConstraints = false
        iwDown.leftAnchor.constraint(equalTo: btnLower.leftAnchor, constant: self.view.frame.size.width / 35).isActive = true
        iwDown.widthAnchor.constraint(equalToConstant:self.view.frame.size.width / 12).isActive = true
        iwDown.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 30).isActive = true
        iwDown.centerYAnchor.constraint(equalTo: btnLower.centerYAnchor,constant: 0).isActive = true
        
        btnLower.addSubview(twLower)
        twLower.translatesAutoresizingMaskIntoConstraints = false
        twLower.leftAnchor.constraint(equalTo: iwDown.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twLower.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 3).isActive = true
        twLower.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twLower.centerYAnchor.constraint(equalTo: btnLower.centerYAnchor,constant: 0).isActive = true
        twLower.font = UIFont.boldSystemFont(ofSize: 25)
        twLower.textAlignment = .center
        twLower.textColor = UIColor(red:163/255, green: 1/255, blue: 1/255,alpha: 1.0)
        twLower.backgroundColor = UIColor.clear
    }
    
    @objc func backBtnClicked(_ sender: UITapGestureRecognizer){
        let touchPoint = sender.location(in: self.view)
        let backBtnClicked = (touchPoint.x < self.view.frame.width/4)
        if(backBtnClicked){
            let startViewController = self.storyboard?.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
            self.present(startViewController, animated: true, completion: nil)
        }
    }
    
    @objc func upDownListener(_ sender: UITapGestureRecognizer){
    }
    
    /*
     * Loads adMob Banner
     */
    public func loadAdMobBanner(){
        var banner:GADBannerView!
        banner = GADBannerView(adSize: kGADAdSizeBanner)
        banner.adUnitID = IdSettings.ADMOB_ID_BANNER
        banner.rootViewController = self
        let req:GADRequest = GADRequest()
        banner.load(req)
        banner.frame = CGRect(x:view.bounds.width/2 - banner.frame.size.width/2 , y:view.bounds.height - banner.frame.size.height,width:banner.frame.size.width, height: banner.frame.size.height)
        view.addSubview(banner)
    }
}
