//
//
//  Category Helper class
//
//  @author Dzh
//  @since 06.03.2018
//

import Foundation
class CategoryHelper{
    
    init(){}
    
    /*
     * Returns category name
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullName(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
        
        case 1: categoryName = "minutes per game"; break;
        case 2: categoryName = "points per game"; break;
        case 3: categoryName = "points per 48 minutes"; break;
        case 4: categoryName = "field goals per game"; break;
        case 5: categoryName = "field goals percentage"; break;
        case 6: categoryName = "three points per game"; break;
        case 7: categoryName = "free throws per game"; break;
        case 8: categoryName = "free throws percentage"; break;
        case 9: categoryName = "points per shot"; break;
            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullNameTitleCase(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
            
        case 1: categoryName = "Minutes per game"; break;
        case 2: categoryName = "Points per game"; break;
        case 3: categoryName = "Points per 48 minutes"; break;
        case 4: categoryName = "Field goals per game"; break;
        case 5: categoryName = "Field goals percentage"; break;
        case 6: categoryName = "Three points per game"; break;
        case 7: categoryName = "Free throws per game"; break;
        case 8: categoryName = "Free throws percentage"; break;
        case 9: categoryName = "Points per shot"; break;
            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryQuestion(category: Int)->String{
        
        var categoryQuestion = ""
        
        switch category {
            
        case 1: categoryQuestion = "Who has more minutes per game?"; break;
        case 2: categoryQuestion = "Who has more points per game?"; break;
        case 3: categoryQuestion = "Who has more points per 48 mins?"; break;
        case 4: categoryQuestion = "Who has more field goals per game?"; break;
        case 5: categoryQuestion = "Who has higher field goals % ?"; break;
        case 6: categoryQuestion = "Who has more 3-points per game?"; break;
        case 7: categoryQuestion = "Who has more free throws per game?"; break;
        case 8: categoryQuestion = "Who has higher free throws % ?"; break;
        case 9: categoryQuestion = "Who has more points per shot?"; break;
            
        default: categoryQuestion = ""; break;
        }
        
        return categoryQuestion
    }
    
    
    
    
}
