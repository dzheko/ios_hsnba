//
// Teamhelper class
//
//  @author Dzh
//  @since 06.03.2018
//
import Foundation

class TeamHelper{
    init(){}
    
    /*
     * Returns team image file
     * @param teamName
     * @return imageName
     */
    static func getTeamImage(teamName: String) -> String {
        
        var imageName = ""
        
    switch teamName {
        case "76ers":                 imageName = "img_philadelphia"; break;
        case "Wizards":               imageName = "img_washington"; break;
        case "Grizzlies":             imageName = "img_memphis"; break;
        case "Bucks":                 imageName = "img_milwaukee"; break;
        case "Chicago Bulls":         imageName = "img_chicago"; break;
        case "Cavaliers":             imageName = "img_cleveland"; break;
        case "Boston Celtics":        imageName = "img_boston"; break;
        case "LA Clippers":           imageName = "img_lac"; break;
        case "GS Warriors":           imageName = "img_gsw"; break;
        case "Atlanta Hawks":         imageName = "img_atalanta"; break;
        case "Miami Heat":            imageName = "img_miami"; break;
        case "Hornets":               imageName = "img_charlotta"; break;
        case "Utah Jazz":             imageName = "img_utah"; break;
        case "Sacramento":            imageName = "img_sacramento"; break;
        case "NY Knicks":             imageName = "img_nyk"; break;
        case "LA Lakers":             imageName = "img_lal"; break;
        case "Orlando Magic":         imageName = "img_orlando"; break;
        case "Mavericks":             imageName = "img_dallas"; break;
        case "Brooklyn Nets":         imageName = "img_brooklyn"; break;
        case "Denver Nuggets":        imageName = "img_denver"; break;
        case "Indiana Pacers":        imageName = "img_indiana"; break;
        case "NL Pelicans":           imageName = "img_orlean"; break;
        case "Pistons":               imageName = "img_detroit"; break;
        case "Raptors":               imageName = "img_toronto"; break;
        case "Rockets":               imageName = "img_houston"; break;
        case "SA Spurs":              imageName = "img_spurs"; break;
        case "Phoenix Suns":          imageName = "img_phoenix"; break;
        case "OKC Thunder":           imageName = "img_okc"; break;
        case "Timberwolves":          imageName = "img_minnesota"; break;
        case "Trail Blazers":         imageName = "img_portland"; break;
        default:                      imageName = "league_all"; break;
    }

        return imageName;
    }
}
