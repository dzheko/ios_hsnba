//
//  SharedPreferences.swift
//  HSNBA
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class SharedPreferences{
    
    static let CURRENT_CATEGORY = "current_category"
    static let HIGHSCORE = "high_score"
    static let WORLDSCORE = "world_score"
    
    init(){}
    
    static func getStringValueForKey(key: String) -> String {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: key) == nil {
            return "";
        } else {
            return preferences.string(forKey: key)!
        }
    }
    
    static func setStringValueForKey(key: String, value: String) {
        let preferences = UserDefaults.standard
        preferences.set(value, forKey: key)
        preferences.synchronize()
    }
    
    static func getIntValueForKey(key: String) -> Int {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: key) == nil {
            return 0;
        } else {
            return preferences.integer(forKey: key)
        }
    }
    
    static func setIntValueForKey(key: String, value: Int) {
        let preferences = UserDefaults.standard
        preferences.set(value, forKey: key)
        preferences.synchronize()
    }
    
    
    
}
