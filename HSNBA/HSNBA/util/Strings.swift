//
//  Strings.swift
//  HSNBA
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class Strings{
    
    static let MENU_ITEMS = "MENU_ITEMS"
    static let BUTTON_CLICKED = "BUTTON_CLICKED"
    
    static let CURRENT_GAME_MODE = "CURRENT_GAME_MODE"
    static let DEFAULT_GAME_MODE = "DEFAULT_GAME_MODE"
    static let LEVEL_GAME_MODE = "LEVEL_GAME_MODE"
    static let MAX_LEVEL = "MAX_LEVEL"
    init(){}
}
