//
//  IdSettings.swift
//  HSNBA
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class IdSettings{
    
    static var APPODEAL_ID = "ea4d2efe3f4c9d6a7132f376e8fe36e01c2f02fa0e20cdf1"
    static var ADMOB_ID_VIDEO = "ca-app-pub-8806065804623360/7811759570"
    static var ADMOB_ID_BANNER = "ca-app-pub-8806065804623360/9407503158"
    static var GOOGLE_ANALYTICS_ID = "UA-83861504-10"
    
    init(){}
    
}
