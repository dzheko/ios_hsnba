//
//  LevelCompleteViewController.swift
//  HSNBA
//
//  Created by leobs on 08.04.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation
import UIKit
import Appodeal

class LevelCompleteViewController: AnalyticsViewController,AppodealBannerViewDelegate{

    let iwBackground : UIImageView = {
        let iwBackground = UIImageView(image: #imageLiteral(resourceName: "slider_back_3"))
        iwBackground.translatesAutoresizingMaskIntoConstraints = false
        return iwBackground
    }()
    
    let twLevel : UILabel = {
        let twLevel = UILabel()
        twLevel.translatesAutoresizingMaskIntoConstraints = false
        twLevel.textColor = .white
        twLevel.font = UIFont.boldSystemFont(ofSize: 18)
        twLevel.textAlignment = .right
        twLevel.backgroundColor = UIColor.clear
        return twLevel
    }()
    
    let twLevelMessage : UILabel = {
        let twLevelMessage = UILabel()
        twLevelMessage.text = "WELL DONE!"
        twLevelMessage.translatesAutoresizingMaskIntoConstraints = false
        twLevelMessage.textColor = .white
        twLevelMessage.font = UIFont.boldSystemFont(ofSize: 30)
        twLevelMessage.textAlignment = .center
        twLevelMessage.backgroundColor = UIColor.clear
        return twLevelMessage
    }()

    let twUserTime : EFCountingLabel = {
        let twUserTime = EFCountingLabel()
        twUserTime.text = "0.0 s"
        twUserTime.translatesAutoresizingMaskIntoConstraints = false
        twUserTime.textColor = .yellow
        twUserTime.font = UIFont.boldSystemFont(ofSize: 60)
        twUserTime.textAlignment = .center
        twUserTime.backgroundColor = UIColor.clear
        return twUserTime
    }()
    
    let twYourTime : UILabel = {
        let twYourTime = UILabel()
        twYourTime.text = "Your time : "
        twYourTime.translatesAutoresizingMaskIntoConstraints = false
        twYourTime.textColor = .white
        twYourTime.font = UIFont.boldSystemFont(ofSize: 25)
        twYourTime.textAlignment = .center
        twYourTime.backgroundColor = UIColor.clear
        return twYourTime
    }()

    let btnBottom = UIButton()
    let twBottomBtn = UILabel()
    var reloadBtnImage = UIImageView()
    var userTime = 22.0
    
    static var isLevelSuccessful = false
    static var levelPlayed = 0
    static var startTime = NSDate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(iwBackground)
        view.addSubview(twLevel)
        view.addSubview(twLevelMessage)
        view.addSubview(twYourTime)
        view.addSubview(twUserTime)
        view.addSubview(btnBottom)
        createUiLayout()
        createBottomBtn()
        
        //Appodeal.setTestingEnabled(true)
        
        let adTypes: AppodealAdType = [.MREC]
        Appodeal.initialize(withApiKey: IdSettings.APPODEAL_ID, types:  adTypes)
        
        let mrec = AppodealMRECView.init(rootViewController: self)!
        mrec.setDelegate(self);
        
        self.view.addSubview(mrec);
        
        mrec.translatesAutoresizingMaskIntoConstraints = false
        mrec.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mrec.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        mrec.heightAnchor.constraint(equalToConstant: mrec.bounds.height).isActive = true
        mrec.topAnchor.constraint(equalTo: twUserTime.bottomAnchor, constant: self.view.frame.size.height/20).isActive = true
        
        mrec.loadAd()
    
        let userTimeDate = Date().timeIntervalSince(LevelCompleteViewController.startTime as Date)
        userTime = NumberFormatter().number(from: String(format: "%.2f", userTimeDate)) as! Double
        //CGFloat(String(format: "%.2f", userTime))
        //print("TIME IS : "+String(describing: time))
        
        twLevel.text = "level : " + String(LevelCompleteViewController.levelPlayed)
        if(LevelCompleteViewController.isLevelSuccessful){
            setSuccessLayout()
        } else {
            setFailureLayout()
        }
        
        let maxLevel = SharedPreferences.getIntValueForKey(key: Strings.MAX_LEVEL)
        
        if(LevelCompleteViewController.isLevelSuccessful &&
           (LevelCompleteViewController.levelPlayed + 1) > maxLevel){
           SharedPreferences.setIntValueForKey(key: Strings.MAX_LEVEL, value: LevelCompleteViewController.levelPlayed + 1)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     * Creates UI Layout
     */
    func createUiLayout(){
        iwBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwBackground.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        iwBackground.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        iwBackground.heightAnchor.constraint(equalToConstant: self.view.frame.size.height).isActive = true
        
        twLevel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twLevel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        twLevel.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/10).isActive = true
        twLevel.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        twLevelMessage.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twLevelMessage.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twLevelMessage.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twLevelMessage.topAnchor.constraint(equalTo: twLevel.bottomAnchor, constant: 0).isActive = true
        
        twYourTime.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twYourTime.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twYourTime.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/24).isActive = true
        twYourTime.topAnchor.constraint(equalTo: twLevelMessage.bottomAnchor, constant: 0).isActive = true
        
        twUserTime.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twUserTime.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twUserTime.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twUserTime.topAnchor.constraint(equalTo: twYourTime.bottomAnchor, constant: 0).isActive = true
        
    }
    
    /*
     * Sets Failure Layout
     */
    func setFailureLayout(){
        twLevelMessage.text = "LEVEL FAILED"
        twBottomBtn.text = "PLAY AGAIN"
        reloadBtnImage.image = UIImage(named:"reload_btn")
        twUserTime.format = "%.2f s"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playAgainClicked(_:)))
        btnBottom.addGestureRecognizer(tap)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: { () -> Void in
            self.twUserTime.countFrom(0.0, to: CGFloat(self.userTime), withDuration: 2)
        })
    }
    
    
    /*
     * Sets Success Layout
     */
    func setSuccessLayout(){
        twLevelMessage.text = "WELL DONE!"
        twBottomBtn.text = "NEXT LEVEL"
        reloadBtnImage.image = UIImage(named:"ic_next_sign")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.nextLevelClicked(_:)))
        btnBottom.addGestureRecognizer(tap)
        
        twUserTime.format = "%.2f s"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: { () -> Void in
            self.twUserTime.countFrom(0.0, to: CGFloat(self.userTime), withDuration: 2)
        })
    }
    
    /*
     * Creates Bottom Btn
     */
    func createBottomBtn(){
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playAgainClicked(_:)))
//        btnBottom.addGestureRecognizer(tap)
//
        btnBottom.translatesAutoresizingMaskIntoConstraints = false
        btnBottom.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btnBottom.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        btnBottom.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        btnBottom.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -self.view.frame.size.height / 15).isActive = true
        
        btnBottom.backgroundColor = .clear
        btnBottom.layer.cornerRadius = 10
        btnBottom.layer.borderWidth = 2
        btnBottom.layer.borderColor = UIColor.white.cgColor
    
        reloadBtnImage = UIImageView(image:#imageLiteral(resourceName: "reload_btn"))
        btnBottom.addSubview(reloadBtnImage)
    
        reloadBtnImage.translatesAutoresizingMaskIntoConstraints = false
        reloadBtnImage.leftAnchor.constraint(equalTo: btnBottom.leftAnchor, constant: self.view.frame.size.width / 35).isActive = true
        reloadBtnImage.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        reloadBtnImage.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        reloadBtnImage.centerYAnchor.constraint(equalTo: btnBottom.centerYAnchor,constant: 0).isActive = true
    
        btnBottom.addSubview(twBottomBtn)
        twBottomBtn.translatesAutoresizingMaskIntoConstraints = false
        twBottomBtn.leftAnchor.constraint(equalTo: reloadBtnImage.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twBottomBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twBottomBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twBottomBtn.centerYAnchor.constraint(equalTo: reloadBtnImage.centerYAnchor,constant: 0).isActive = true
        twBottomBtn.font = UIFont.boldSystemFont(ofSize: 23)
        twBottomBtn.textAlignment = .left
        twBottomBtn.textColor = UIColor.white
        twBottomBtn.text = "PLAY AGAIN"
        twBottomBtn.backgroundColor = UIColor.clear
    }
    
    @objc func playAgainClicked(_ sender: UITapGestureRecognizer) {
        sendEvent(category: "LEVEL VIEW BTNS", action: "PLAY AGAIN", label: "PLAY AGAIN")
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        ViewController.currentScore = 0
        EndViewController.numberOfAttempts = 0
        ViewController.currentLevel = LevelCompleteViewController.levelPlayed
        LevelCompleteViewController.startTime = NSDate()
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func nextLevelClicked(_ sender: UITapGestureRecognizer) {
        sendEvent(category: "LEVEL VIEW BTNS", action: "NEXT LEVEL", label: "NEXT LEVEL")
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        ViewController.currentScore = 0
        ViewController.currentLevel = LevelCompleteViewController.levelPlayed + 1
        EndViewController.numberOfAttempts = 0
        LevelCompleteViewController.startTime = NSDate()
        self.present(viewController, animated: true, completion: nil)
    }
    
}

